SRI RAMANA MAHARSHI
1284. With the mind turned inward, drown the world in the great void, dispel illusion. Beholding then the void as void, destroy the void by drowning it in the deep ocean of Self-awareness.
1285. Those well established in the Self will never pursue the world's vile ways. For such descent into the false allurements of the world is yielding to the animal weakness for sense-pleasure.
1286. Not in one single thing on earth can happiness be found. How could the muddled mind delude itself and think that happiness can be derived from objects in this world?
1287. Fond, foolish people may find joy in pleasure at the moment. Soon it palls and leaves but pain behind.
1288. Those who desire and like and live the trivial life the ego knows reject as if it were unreal the natural life of infinite bliss within their own hearts ever present for their enjoyment.
1289. Those who enjoy the ego's life false phenomena perish and die. The state of grace, supreme Awareness, the life lived in Self-Being, this alone is bliss worth seeking.
1290. Poor seer who suffer endlessly because you still perceive the object, not the subject, please look inward, not without, and taste the bliss of non-duality.
1291. Running out search of wonders and dancing there with pleasure, do not perish. Better with the light of grace, look, look within, and find certitude in being and biding as your true Self.
1292. Those who diving deep within have found the Self have nothing else to know. And why? Because they have gone themselves beyond all forms and are Awareness without form.
1293. Investigating who perceives this false external world of sense, bring to an end the frisking ego's mischief. Abiding as supreme awareness in the heart, this alone is liberation.
1294. One who has through supreme devotion gained one's true Being as Awareness can recognize no other state except this; one's natural state of being one supreme Awareness.
1295. Sense-pleasures sought and found by blind, unguarded fools are fit only for contempt by those who long to taste the rich, ripe fruit of Bliss supreme immeasurably sweet.
1296. Absence of mental craving for sense-enjoyments is true fasting. Abidance in the Self is worship true, hence, those with pure, clear wisdom cherish as most precious this fasting and this worship.
1297. Better the state of inner peace and Self-abidance where no thought arises than attainment of the power to bring about fulfillment, prompt and sure, of every wish.
1298. Since That which is shines bright within as I, the Self is but Awareness. To search in the heart and find the Self, the best help is the inescapable light of the one Self alone.
1299. The world should not occupy one's mind.
1300. The wise renounce at heart and quite forget the wondrous charms of this false world which only ruins those who trust it.
1301. That which is for ever shines in grace as I, the Self, the Heart. Can That be blamed for lacking Grace? The fault is theirs who do not turn within and seek the Self in love.
1302. The universal eye avoids no creature. We are blind, for we look outward, not within.
1303. Those who have known the Self aright, instead of wandering in the world, abide in their own natural state.
1304. This worldly life of false phenomena full of fear is sinking deeper in illusion, not authentic living.

SRI ANNAMALAI SWAMI
1305. Questioner: The outside world is a miserable, confusing place. There is not much going on there that helps us to remember who we really are. Annamalai Swami: Yes, you can says that this state of affairs is also Bhagavan's grace, Bhagavan's compassion. You could says that He keeps the world like this as an incentive to go inwards. This state of affairs sets up a real choice; if we go outwards there are problems; if we go inwards there is peace.
1306. We have to give up all the things of this world, and all other worlds, and direct all our attention towards the Self. If we want anything in this world or the next our energy will be dispersed in these desires, and to fulfill these desires we shall have to be reborn again and again.
1307. You are going to different places on a pilgrimage, but what you are really looking for is you yourself. You cannot achieve success in this by going on external searches because you yourself are the one that is being looked for. Your real nature is peace. Forgetting this, you have lost your peace and you are searching in the outside world where there is no peace to be found.
1308. To find the Self, to find what is real, you have to look inside yourself.
1309. Mind is not improved by long journeys to far-flung places. Instead, make an internal pilgrimage. Take the mind back to its source and plunge it into the peace-giving waters of the Self. If you once make this pilgrimage, you will never need to go looking for happiness or peace in any other place.
1310. If you are not able to find this Self within yourself, you will not find it anywhere else. Searching on the outside and visiting holy places will not help you.
1311. Many people are visiting swamis, temples and holy places. Doing these things will not yield any good fruit. For real and lasting results you have to look inside yourself and discover the Self within. You can do that anywhere.
1312. Turn to the light within all the time.
1313. You dissipate your desire for the Self by undertaking all kinds of useless activities that waste your time and lead to attachments. You think that your life is endless and that you can put off meditation till a later date. With this kind of attitude, you will die filled with regrets, not filled with peace.

SRI SADHU OM
1314. The more we attend to thought pertaining to the second and third persons, the more they will increase.
1315. There are two kinds of impediments which act as obstacles for the mind to achieve Self-abidance, and hence two kinds of strength of mind are essential for overcoming them. The first strength is that which is required to prevent the mind from branching out into innumerable thoughts through the force of tendencies towards the sense-objects. The second strength is that which is required to direct the mind (the power of attention) towards the first person or Self, that is, the strength actually to attend to Self.
1316. The nature of the mind is to attend always to things other than itself, that is, to know only second and third persons. If the mind in this way attend to a thing, it means that it is clinging (attaching itself) to that thing. Attention itself is attachment!
1317. When the power of attention of the mind is directed more and more towards second and third person objects, both the strength to attend to those objects and the ignorance - five sense-knowledges in the form of thoughts about them - will grow more and more.
1318. The mind which attend to Self is no more the mind; it is the consciousness aspect of Self! Likewise, so long as it attends to the second and thirds persons (the world), it is not the consciousness aspect of the Self; it is the mind.
1319. Many are those who take qualified experiences of taste, light, sound and so on to be the final attainment of Self-knowledge and because they have had these experiences they think that they have attained liberation and they become more and more entangled in attention to second and third persons, thus losing their foothold on Self-attention. Such aspirants are called 'those fallen from yoga.' This is similar to a man bound for Delhi getting down from the train at some intermediate station, thinking 'Verily, this is Delhi', being deluded by its attractive grandeur!
1320. A sincere aspirant should arrange his work in such a way that he will spend only a portion of his time and energy for maintaining the body, so that he can utilize the remaining time and energy in striving to earn the great profit of Self-knowledge.

SRI MURUGANAR
1321. When we mistake that which is impermanent for that which is enduring, it only serves to emphasize the disharmony within our hearts. The true temperament is one that cleaves to the indestructible Self dwelling at the heart of our very existence as the immovable reality.
1322. Unless the mind subsides into the heart, whose nature is consciousness, and experiences the deep peace of union with it, the mind, through separation from it, will fall into the trap of the sense organs, be whirled about in the world of the senses, and become scattered.
1323. Those who crave a worldly life of pomp and ostentation, relying on the fleeting impressions of the sense organs, will remain slaves to the obscuring desires of the flesh, and give scant regard to the eternal life of living as the supreme.
1324. Those who, instead of enjoying the bliss of the Heart - the form of consciousness - embrace and find pleasure in the objects of sense, will wander from birth to death to birth, alternating in that deluded consciousness between the states of remembering and forgetting.

SRI NISARGADATTA MAHARAJ
1325. I see what you too could see, here and now, but for the wrong focus if your attention. You give no attention to your Self. Your mind is all with things, people and ideas, never with your Self. Bring your Self into focus, become aware of your own existence.
1326. True happiness cannot be found in things that change and pass away. Pleasure and pain alternate inexorably. Happiness come from the Self and can be found in the Self only.
1327. Pain is the background of all your pleasures. You want them because you suffer. On the other hand, the very search for pleasure is the cause of pain. It is a vicious circle.
1328. It is all a matter of focus. Your mind is focused in the world; mine is focused in reality.
1329. Deliberate daily exercise in discrimination between the true and the false and renunciation of the false is meditation.
1330. Look to yourself for the permanent. Dive deep within and find what is real in you.
1331. The world is the abode of desires and fears. You cannot find peace in it. For peace you must go beyond the world.
1332. To know the world you forget the Self – to know the Self you forget the world. What is world after all? A collection of memories. Cling to one thing, that matters, hold on to "I am" and let go all else.
1333. The Self is beyond both, beyond the brain, beyond the mind.
1334. If you leave it to time, millions of years will be needed. Giving up desire after desire is a lengthy process with the end never in sight. Leave alone your desires and fears, give your entire attention to the subject, to him who is behind the experience of desire and fear.
1335. Turn within and you will come to trust yourself.
1336. Nothing stop you but preoccupation with the outer which prevents you from focusing the inner.
1337. Sooner or later you are bound to discover that if you really want to find, you must dig at one place only - within.
1338. Playing with various approaches may be due to resistance to going within, to the fear of having to abandon the illusion of being something or somebody in particular. To find water you do not dig small pits all over the place, but drill deep in one place only. Similarly, to find your Self you have to explore yourself.
1339. The world is full of contradictions, hence your search for harmony and peace. These cannot find in the world, for the world is the child of chaos. To find order you must search within.
1340. What happens to the body and the mind may not be within your power to change, but you can always put an end to your imagining yourself to be body and mind. Whatever happens, remind yourself that only your body and mind are affected, not yourself.
1341. To eschew the unnecessary us austerity.
1342. Having things under control at all times is austerity.
1343. It is the choices you make that are wrong. To imagine that some little thing - food, sex, power, fame - will make you happy is to deceive yourself. Only something as vast and deep as your real Self can make you truly and lastingly happy.
1344. Your difficulty lies in your wanting reality and being afraid of it at the same time. You are afraid of it because you do not know it. The familiar things are known, you feel secure with them.
1345. As long as your focus is on the body, you will remain in the clutches of food, sex, fear and death. Find yourself and be free.
1346. Abandon all ideas about yourself and you will find yourself to be the pure witness, beyond all that can happen to the body or the mind.
1347. The body and the mind are only symptoms of ignorance, of misapprehension.
1348. As long as the mind is busy with its contortions, it does not perceive its own source.
1349. By its very nature the mind is outward turned; it always trends to seek for the source of things among the things themselves; to be told to look for the source within, is, in a way, the beginning of a new life.
1350. Renunciation of the false is liberating and energizing. It lays open the road to perfection.
1351. It is the clinging to the false that makes the true so difficult to see.
1352. When you want something ask yourself: Do I really need it?" and if the answer is no, then just drop it.
1353. Freedom comes through renunciation. All possession is bondage.
1354. Maharaj: Stay whit the changeless among the changeful, until you are able to go beyond. Questioner: When will it happen? Maharaj: It will happen as soon as you remove the obstacles. Questioner: Which obstacles? Maharaj: Desire for the false and fear of the true.
1355. Questioner: In the beginning we may have to pray and meditate for some time before we are ready for Self-inquiry. Maharaj: If you believe so, go on. To me, all delay is a waste of time. You can skip all the preparation and go directly for the ultimate search within. Of all the Yogas it is the simplest and the shortest.
1356. The innermost light, shining peacefully and timelessly in the heart, is the real Guru.
1357. I know myself as I am - timeless, spaceless, causeless. You happen not to know, being engrossed as you are in other things.
1358. Limit your interests and activities to what is needed for you and your dependents barest needs. Save all your energies and time for breaking the wall your mind had built around you.
1359. All you need is already within you, only you must approach your Self with reverence and love.
1360. Now, go within, into a state which you may compare to a state of waking sleep, in which you are aware of yourself, but not of the world. In that state you will know, without the least trace of doubt, that at the root of your being you are free and happy.
1361. The clarification and purification needed at the very start of the journey, only awareness can give. Love and will shall have their turn, but the ground must be prepared. The sun of awareness must rise first - all else will fallow.
1362. Awareness is always with you. The same attention that you give to the outer, you turn to the inner. No new, or special kind of awareness is needed.
1363. As long as you are engrossed in the world. you are unable to know yourself: to know yourself, turn away your attention from the world and turn it within.
1364. You must seek the Self and having found it, stay with it.
1365. The body and the mind are limited and therefore vulnerable; they need protection which gives rise to fear. As long as you identify yourself with them you are bound to suffer; realize your independence and remain happy. I tell you, this is the secret of happiness. To believe that you depend on things and people to happiness is due to ignorance of your true nature; to know you need nothing to be happy, except Self-knowledge, is wisdom.
1366. The world appears to you so overwhelmingly real, because you think of it all the time; cease thinking of it and it will dissolve into thin mist.
1367. Self-awareness is Yoga.

THE SUPREME YOGA
1368. None of the objects in this world is meant to give happiness to anyone. The mind vainly seeks to find such happiness in the objects of this world.
1269. This perception of defects of the world has destroyed the undesirable tendencies in my mind; and therefore, desire for sense-pleasure does not arise in my mind.
1270. I am constantly inquiring: "How can I wean my heart completely away from even thinking of this ever-changing phantasm called the world?"
1371. All that is good and auspicious flows from self-control. All evil is dispelled by self-control.
1372. As long as one is not satisfied in the Self, he will be subjected to sorrow. With the rise of contentment the purity of one's heart blooms.
1373. This Self is neither far nor near; it is not inaccessible nor this is it in distant place: it is what in oneself appears to be the experience of bliss, and is therefore realized in oneself.
1374. He who does not allow his mind to roam in objects of pleasure is able to master it.
1375. Ignorance or mental conditioning is acquired by man effortlessly and it seems to promote pleasure, but in truth is the giver of grief. It creates a delusion of pleasure only by the total veiling of Self-knowledge.
1376. If the mind turns towards the truth, it abandons its identification with the body and attains the supreme.
1377. As long as the objective universe is perceived one does not realize the Self.
1378. One should abandon all cravings for pleasure and attain wisdom. Only the mind that has been well disciplined really experiences happiness.
1379. The reality is the one infinite consciousness which does not undergo any change.
1380. In truth, only the indivisible and unmodified consciousness exists.
1381. They are the true heroes who have brought under control the mind which is dominated by ignorance and delusion.
1382. Rest in peace and purity like the ocean when it is not agitated by wind.
1383. Do not let your mind wander among the objects of the world. You yourself are the supreme Self, the infinite consciousness; you are naught else!
1384. He who runs after objects created by his own mind surely comes to grief.
1385. In this ocean of ignorant mental conditioning, he who has found the raft of Self-knowledge is saved from drowning: he who has not found that raft is surely drowned.
1386. They who are busy with the diverse affairs in this world in pursuit of pleasure and power, do not desire to know the truth which they obviously do not see.
1387. O mind, abandon your craving for sense-pleasures so that you may be rid of the miseries of repeated old age and death.
1388. The mind is not subdue without persistent practice. Hence, take up this practice of renunciation. Until one turns away from sense-pleasure here, one will continue to roam in this world of sorrow. Even a strong man will not reach his destination if he does not move towards it: no one can reach the state of total dispassion without persistent practice.
1389. When you turn completely away from the pursuit of pleasure, then you attain to the supreme state through the means of inquiry.
1390. I shall give up everything and with my mind completely withdrawn from the pursuit of pleasure, I shall remain happily established in the Self.
1391. Abandon the desire for the essenceless and useless sense-pleasure in this world. The attractive objects that tempt you here do not deserve your admiration.
1392. In whatever the mind tends to sink, retrieve it from it and direct towards the truth. Thus will the wild elephant of the mind be tamed.
1393. The ghost of delusion afflicts one only as long as Self-knowledge does not arise in him.
1394. Until we attain Self-knowledge, we shall return again to this plane of birth and death to undergo childhood, youth, manhood, old age and death again and again, we shall engage ourselves in the same essenceless actions and experiences. Cravings destroy wisdom. Lost in satisfying sensual appetites, life ebbs away fast.
1395. Suffering flows towards those who are mentally conditioned. This whole creation is thus pervaded by ignorance.
1396. This body can have no relationship whatsoever with the Self.
1397. When one is ignorant, one entertains the wrong notion that the body is the Self; his own senses prove to be his worst enemies.
1398. In the absence of the "taste" (direct knowledge) of the cosmic intelligence, the senses endeavor to apprehend their objects and vainly imagine that such contact gives rises to meaningful experience!
1399. Even as a caged bird is unable to find freedom, the ignorant man devoted to the fulfillment of his appetites is unable to find release from bondage. His mind which is befuddled with apparently countless tendencies and conditioning is unable to see clearly the revolving wheel of life and death.
1400. In the darkness of ignorance, the fool thinks he experiences pleasure or happiness in the objects of this world.
1401. All these objects have a beginning and an end, they are limited, they are perishable.
1402. Liberation is attained when one arrives at the state of supreme peace after intelligent inquiry into the nature of the Self and after this has brought about an inner awakening.
1303. Awakening of inner intelligence destroys ignorance.
1404. Eternal good is not to be found in any of the activities of any of the senses.
1405. Establish your mind, which flits from one thing to another, firmly in your heart.
1406. the wise one restrains the senses and remains centered in the Self.
1407. The fool who revels in pleasure invites sorrow and misfortune.
1408. The fullness of perfection begins with the effectiveness of self-discipline or the abandonment of the pursuit of pleasure.
1409. It is a great misfortune to pursuit pleasure.
1410. Investigate the truth with the help of direct experience; behold the primordial truth by direct experience. One who abandons this experience and runs after illusory "realities" is a fool. SRI SANKARA (1411 -1438)
1411. When he has lost sight of his true Self, immaculate and resplendent, a man identifies himself with his body out of ignorance.
1412. Identification of oneself with the body is the seed of the pain of birth etc. in people attached to the unreal, so get rid of it with care. When this thought is eliminated, there is no more desire for rebirth.
1413. There is no self-identification with such things as the body for a liberated man. There is no being awake for someone asleep, nor sleep for someone awake, for these two state are by their very nature distinct.
1414. A wise man attains peace by recognizing his own true nature as undifferentiated awareness.
1415. Knowing his true indivisible nature by his own realization the perfected man should remain in full possession of himself free from imaginations within.
1416. When the mass of desires for things other than oneself, obscuring the contrary desire for one's real Self, are eliminated by constant Self-remembrance, then it discloses itself of this own accord.
1417. The fruit of knowledge should be the turning away from the unreal, while turning towards the unreal is seen to be the fruit of ignorance.
1418. Established in meditation, with the senses controlled, the mind calmed and continually turned inwards, destroy the darkness of beginningless ignorance by recognizing the oneness of Reality.
1419. To overcome the outward-turning power of the mind is hard to accomplish without completely eliminating the veiling effect, but covering over one's inner Self can be removed by discrimination between seer and objects.
1420. Absence of an barrier is finally unquestionable when there is no longer any distraction caused by illusory objects.
1421. When the mind loses its direction towards its goal and becomes outward-turned its run from one thing to another, like a play-ball carelessly dropped on the steps of some stairs.
1422. He who is devoted to meditating on the Truth attains the eternal glory of his true nature, while he who delights in dwelling on the unreal perishes.
1423. Dwelling on externals increases the fruit of superfluous evil desires for all sorts of things, so wisely recognizing this fact, one should abandon externals and cultivate attention to one's true nature within.
1424. As the mind becomes more and more inward - turned it becomes gradually freed from external desires, and when all such desires are fully eliminated Self-realization is completely freed from obstruction.
1425. Seers know this supreme Reality, free from the distinctions of knower, known and knowledge, infinite, complete in itself and consisting of pure Awareness.
1426. This is supreme Reality is non-dual in the absence of any other reality beside itself. In the state of knowledge of ultimate truth there is nothing else.
1427. Completely rooting out desire for the senses and abandoning all activity by one-pointed devotion to liberation, he who is established with true faith in study etc. purges away the passion from this understanding.
1428. The Self itself is pure consciousness.
1429. Through enjoyment of unreal things, there is no contentment at all, nor any getting rid of pain. therefore contented by enjoying the essence of secondless bliss, stand thou rejoicing, resting on the Self that is true Being.
1430. Attracted by the Self the man goes to the being of the Self by resting on it alone.
1431. Perfect discernment, born of clear awakening, arises free from doubt, and pure of all bondage, where there is propelling power towards of all bondage, where once the division is made between the real natures of the seer and what is seen.
1432. Attachment to the outwards brings as it fruit the perpetual increase of evil mind-images. Knowing this and putting away outward things by discernment, let him place his attachment in the Self forever.
1433. He whose delight is attachment to the real, freed, he gains the greatness of the Self, eternal; but he who delights in attachment to the false, perishes.
1434. Transcending every, visible object of sense, fixing the mind on pure being, the totality of bliss, with right intentness within and without, pass the time.
1435. The motion of enticement to sensual objects is the cause of world-bondage, through attachment to what is other than Self.
1436. By resting ever in the Self, the restless mind of him who seeks union is stilled, and all imaginings fade away.
1437. As the mind rests more and more on the Self behind it, it is more and more freed from outward imaginings; when imaginings are put away, and no residue left, he enters and becomes the Self.
1438. The knowledge of the real by the eye of clear insight is to be gained by one's own sight and not by the teacher's


