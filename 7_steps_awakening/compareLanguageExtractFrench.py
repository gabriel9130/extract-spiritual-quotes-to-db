import re

def step_to_file(step, index_begin, filename):
    step_reformat = ""
    for sage, quotes in step.items():
        step_reformat += sage + "\n"
        for quote in quotes:
            step_reformat += str(index_begin) + ". " + quote + "\n"
            index_begin = index_begin + 1
        step_reformat += "\n"

    with open(filename, "w", encoding="utf8") as text_file:
        print(step_reformat, file=text_file)


def extract_quotes(step_file):
    sages = ["SRI RAMANA MAHARSHI", "SRI SADHU OM", "SRI MURUGANAR", "SRI ANNAMALAI SWAMI", "SRI NISARGADATTA MAHARAJ",
             "THE SUPREME YOGA", "SRI SANKARA"]
    step_quotes = {}
    with open(step_file, encoding="utf8") as fp:
        line = fp.readline()
        while line:
            line = line.replace("\n", "").strip()
            if line != "":
                if line in sages:  #new sage section
                    step_quotes[line] = []
                    current_sage = line
                    fp.readline()  # skip line after sage name (---------------)
                else:  #quote
                    if re.match("^[0-9]+.", line):  # new quote
                        line = fp.readline() #skip english quote
                        step_quotes[current_sage].append(line.strip())
                    else:  # the rest of the previous quote
                        step_quotes[current_sage][-1] += " " + line.strip()
            line = fp.readline()
    return step_quotes

def main():
    step_quotes = extract_quotes("translation/correctedTranslation/compare_step2_corrected.txt")
    step_to_file(step_quotes, 214, "translation/correctedTranslation/compare_step2_fr_extracted.txt")


if __name__ == '__main__':
    main()
