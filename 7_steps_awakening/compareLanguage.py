import re

def two_steps_to_file(step_en, step_fr, filename):
    step_reformat = ""
    quote_index = 853
    for sage in step_en:
        step_reformat += sage + "\n"
        step_reformat += "-------------------------------------------------------------------------------------" + "\n"
        quotes_en = step_en[sage]
        quotes_fr = step_fr[sage]
        for i in range(len(quotes_en)):
            step_reformat += str(quote_index) + "." + quotes_en[i] + "\n" + quotes_fr[i] + "\n\n"
            quote_index = quote_index + 1
        step_reformat += "\n" + "\n"

    with open(filename, "w") as text_file:
        print(step_reformat, file=text_file)


def extract_quotes(step_file):
    sages = ["SRI RAMANA MAHARSHI", "SRI SADHU OM", "SRI MURUGANAR", "SRI ANNAMALAI SWAMI", "SRI NISARGADATTA MAHARAJ",
             "THE SUPREME YOGA", "SRI SANKARA"]
    step_quotes = {}
    with open(step_file, "r") as fp:
        line = fp.readline()
        while line:
            line = line.replace("\n", "").strip()
            if line != "":
                if line in sages:  # new sage section
                    step_quotes[line] = []
                    current_sage = line
                else:  # quote
                    if re.match("^[0-9]+.", line):  # new quote
                        line = re.sub("^[0-9]+.", "", line).strip()
                        step_quotes[current_sage].append(line)
                    else:  # the rest of the previous quote
                        step_quotes[current_sage][-1] += " " + line
            line = fp.readline()
    return step_quotes

def main():
    quotes_fr = extract_quotes("step_5_fr.txt")
    quotes_en = extract_quotes("step_5.txt")
    two_steps_to_file(quotes_en, quotes_fr, "compare_step5.txt")


if __name__ == '__main__':
    main()
