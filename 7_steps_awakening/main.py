import re
import sqlite3

conn = sqlite3.connect('3_7_steps_to_awakening.db')

def step_to_file(step, filename):
    step_reformat = ""
    for sage, quotes in step.items():
        step_reformat += sage + "\n"
        for quote in quotes:
            step_reformat += quote + "\n"
        step_reformat += "\n"

    with open(filename, "w") as text_file:
        print(step_reformat, file=text_file)


def extract_quotes(step_file):
    sages = ["SRI RAMANA MAHARSHI", "SRI SADHU OM", "SRI MURUGANAR", "SRI ANNAMALAI SWAMI", "SRI NISARGADATTA MAHARAJ",
             "THE SUPREME YOGA", "SRI SANKARA"]
    step_quotes = {}
    with open(step_file, "r", encoding="utf8") as fp:
        line = fp.readline()
        while line:
            line = line.replace("\n", "").strip()
            if line != "":
                if line in sages:  # new sage section
                    step_quotes[line] = []
                    current_sage = line
                else:  # quote
                    if re.match("^[0-9]+.", line):  # new quote
                        line = re.sub("^[0-9]+.", "", line).strip()
                        step_quotes[current_sage].append(line)
                    else:  # the rest of the previous quote
                        step_quotes[current_sage][-1] += " " + line
            line = fp.readline()
    # step_to_file(step_quotes, step_file.replace(".txt", "_output.txt"))
    return step_quotes

def save_quotes_to_db(step_quotes, step_number):
    chapter_by_step = {
        1: "Use the quotes as practise instructions. Know that a conceptual journey is not a journey to Awakening.",
        2: "Know that the world is a dreamlike illusion.",
        3: "See how the impostor self perpetuates its imaginary self and all illusion and suffering.",
        4: "Increase your desire for liberation.",
        5: "Be inspired, encouraged and motivated.",
        6: "Turn your attention inward.",
        7: "Practise the most rapid, effective and direct method that brings the impostor self to its final end."
    }
    # chapter_by_step = {
    # 1: "Utilisez les citations comme instructions de pratique. Sachez qu'un cheminement conceptuel n'est pas un cheminement vers l'Éveil.",
    # 2: "Sachez que le monde est une illusion semblable à un rêve.",
    # 3: "Voyez comment le soi imposteur perpétue son soi imaginaire et toutes les illusions et souffrances.",
    # 4: "Augmentez votre désir de libération.",
    # 5: "Soyez inspiré, encouragé et motivé.",
    # 6: "Tournez votre attention à l'intérieur.",
    # 7: "Pratiquez la méthode la plus rapide, efficace et directe pour mettre une fin finale au soi imposteur."

    begin_quote_number_by_step = {1: 1, 2: 214, 3: 525, 4: 786, 5: 853, 6: 1284, 7: 1439}
    c = conn.cursor()
    index_quote = 0
    for sage, quotes in step_quotes.items():
        for quote in quotes:
            try:
                sql_query = "INSERT INTO SpiritualQuotes (quote, author, source, language, chapter, number) VALUES ('%s', '%s', '%s', '%s', '%s', %d);" \
                    % (quote.replace("'", "''"), sage.replace("SRI ", "").title(), "The Seven Steps to Awakening", "en", chapter_by_step[step_number], begin_quote_number_by_step[step_number] + index_quote)
                c.execute(sql_query)
            except Exception as e:
                print(e)
            index_quote = index_quote + 1
    conn.commit()

def seven_steps_to_awakening_to_db():
    for i in range(1, 8):
        #./translation/correctedTranslation/compare_step{}_fr_extracted.txt
        step_quotes = extract_quotes("./steps/step_{}.txt".format(i))
        save_quotes_to_db(step_quotes, i)

def main():
    seven_steps_to_awakening_to_db()
    conn.close()


if __name__ == '__main__':
    main()
