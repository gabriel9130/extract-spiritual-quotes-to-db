SRI ANNAMALAI SWAMI
-------------------------------------------------------------------------------------
786.The desire for enlightenment is necessary because without it you will never take the necessary steps to realize the Self. A desire to walk to a particular place is necessary before you take any steps. If that desire is not present, you will never take the first step. When you realize the Self, that desire will go.
Le désir d'illumination est nécessaire car sans lui, vous ne pourrez jamais prendre les mesures nécessaires pour réaliser le Soi. Il faut avoir envie de marcher jusqu'à un endroit particulier avant de faire les démarches. Si ce désir n'est pas présent, vous ne ferez jamais le premier pas. Lorsque vous réalisez le Soi, ce désir disparaîtra.

787.If intensity to know yourself is strong enough, the intensity of your yearning will take you to the Self.
Si l'intensité de la connaissance du Soi est assez forte, l'intensité de votre désir vous mènera au Soi.

788.Your most important objective must be realizing the Self. If you have not done this, you will spend your time in ignorance and illusion.
Votre objectif le plus important doit être de réaliser le Soi. Si vous ne l'avez pas fait, vous passerez votre temps dans l'ignorance et l'illusion.



SRI SADHU OM
-------------------------------------------------------------------------------------
789.The mind which has obtained a burning desire for Self-attention, which is Self-inquiry, is said to be the fully mature one.
On dit que l'esprit qui a obtenu un désir brûlant d'attention au Soi, c'est-à-dire d’investigation du Soi, est celui qui a atteint sa pleine maturité.

790.Since this mind, which has very well understood that the consciousness which shines as 'I' alone is the source of full and real happiness, now seeks Self because of its natural craving for happiness, this intense desire to attend to Self is indeed the highest form of devotion.
Puisque cet esprit, qui a très bien compris que la conscience qui brille comme "je" seul est la source du bonheur plein et réel, recherche maintenant le Soi à cause de son besoin naturel de bonheur, ce désir intense de suivre le Soi est en effet la plus haute forme de dévotion.

791.In order to qualify as an aspirant, one must have the absolute conviction that happiness, the sole aim of all living beings, can be obtained not from external objects but only from one's own inmost Self. When one has this qualification, an intense yearning will arise in one's heart to try to attend to an know Self. Indeed, for a true aspirant the desire and effort to know Self will become the most important part of his life, and all other things will be regarded as being only of secondary importance. When such an intense yearning arise in one, success is assured, for 'where there is a will there is a way'.
Pour être aspirant, il faut avoir la conviction absolue que le bonheur, seul but de tous les êtres vivants, peut être obtenu non pas à partir d'objets extérieurs, mais uniquement à partir de son propre Soi intime. Lorsqu'on a cette qualification, un désir intense naît dans le cœur pour essayer de suivre le Soi connu. En effet, pour un véritable aspirant, le désir et l'effort de connaître le soi deviendront la partie la plus importante de sa vie, et toutes les autres choses seront considérées comme n'ayant qu'une importance secondaire. Lorsqu'un désir aussi intense se manifeste, le succès est assuré, car "là où il y a une volonté, il y a un moyen".

792.Mature aspirants will willingly and without rebelling submit themselves to this magnetic power of the Grace of Self-effulgence. Others, on the other hand, will become extroverted (that is, will turn their attention outwards) fearing the attraction of this power. Therefore, we should first make ourself fit by the intense love to know the Self and by tremendous detachment of having no desire to attend to any second or third person.
Les aspirants matures se soumettront volontairement et sans se rebeller à ce pouvoir magnétique de la grâce du rayonnement du Soi. D'autres, en revanche, deviendront extravertis (c'est-à-dire qu'ils tourneront leur attention vers l'extérieur) de peur de l’attraction de ce pouvoir. Par conséquent, nous devrions d'abord nous préparer par l'amour intense de la connaissance du Soi et par le détachement énorme de n'avoir aucun désir de s'occuper d'une deuxième ou d'une troisième personne.



SRI MURUGANAR
-------------------------------------------------------------------------------------
793.Grace, the supreme, is rare and unique. There is nothing that resembles it. to make it the object of one's desire is the most virtuous of desires. As all other desires are quenched through the very desire for it, it will shine in the heart spontaneously.
La grâce, le suprême, est rare et unique. Il n'y a rien qui lui ressemble. En faire l'objet de son désir est le plus vertueux des désirs. Comme tous les autres désirs sont assouvis par ce même désir, il brillera spontanément dans le cœur.



SRI NISARGADATTA MAHARAJ
-------------------------------------------------------------------------------------
794.Earnestness is the only condition of success.
Le sérieux est la seule condition du succès.

795.All will happen as you want it, provided you really want it.
Tout se passera comme vous le voulez, à condition que vous le vouliez vraiment.

796.Want the best. The highest happiness, the greatest freedom.
Vouloir le meilleur. Le plus grand bonheur, la plus grande liberté.

797.Your earnestness will determine the rate of progress.
Votre sérieux déterminera le rythme des progrès.

798.It is enough to stop thinking and desiring anything, but the Supreme.
Il suffit d'arrêter de penser et de désirer quoi que ce soit, sauf le Suprême.

799.The idea of enlightenment is of utmost importance. Just to know that there is such a possibility changes one's entire outlook.
L'idée d'illumination est de la plus haute importance. Le simple fait de savoir qu'une telle possibilité existe change toute la perspective d'une personne.

800.If you are truly earnest and honest, the attainment of reality will be yours.
Si vous êtes vraiment sérieux et honnête, la réalisation de la réalité vous appartiendra.

801.Whatever name you give it: will, or steady purpose, or one-pointedness of the mind, you come back to earnestness, sincerity, honesty. When you are in dead earnest, you bend every incident, every second of your life to your purpose. You do not waste time and energy on other things. You are totally dedicated, call it will, or love, or plain honesty.
Quel que soit le nom que vous lui donnez : volonté, ou objectif constant, ou centration mentale en un point, vous revenez au sérieux, à la sincérité, à l'honnêteté. Quand vous êtes sérieux, vous faites en sorte que chaque incident, chaque seconde de votre vie, serve votre objectif. Vous ne perdez pas de temps et d'énergie pour d'autres choses. Vous êtes totalement dévoué, appelez cela de la volonté, ou de l'amour, ou de l'honnêteté pure.

802.We are complex beings, at war within and without. We contradict ourselves all the time, undoing the work of yesterday. No wonder we are stuck. A little of integrity would make a lot difference.
Nous sommes des êtres complexes, en guerre à l'intérieur et à l'extérieur. Nous nous contredisons sans cesse, défaisant le travail d'hier. Pas étonnant que nous soyons coincés. Un peu d'intégrité ferait une grande différence.

803.The desire to find the Self will be surely fulfilled, provided you want nothing else. But you must be honest with yourself and really want nothing else. If in the meantime you want many other things and are engaged in their pursuit, your main purpose may be delayed until you grow wiser and cease being torn between contradictory urges. Go within, without swerving, without ever looking outward.
Le désir de trouver le Soi sera sûrement comblé, à condition de ne rien vouloir d'autre. Mais vous devez être honnête avec vous-même et ne rien vouloir d'autre. Si, entre-temps, vous voulez beaucoup d'autres choses et êtes engagé dans leur poursuite, votre objectif principal peut être retardé jusqu'à ce que vous deveniez plus sage et que vous cessiez d'être déchiré par des pulsions contradictoires. Allez à l'intérieur, sans dévier, sans jamais regarder vers l'extérieur.

804.Merely to trust is not enough. You must also desire. Without desire for freedom of what use is the confidence that you can acquire freedom? Desire and confidence must go together. The stronger your desire, the easier comes the help.
Il ne suffit pas de faire confiance. Vous devez aussi désirer. Sans désir de liberté, à quoi sert la confiance que vous pouvez acquérir la liberté ? Le désir et la confiance doivent aller ensemble. Plus votre désir est fort, plus l'aide est facile.

805.The greatest Guru is helpless as long as the disciple is not eager to learn.
Le plus grand gourou est impuissant tant que le disciple n'est pas désireux d'apprendre.

806.Eagerness and earnestness are all-important. Confidence will come with experience. Be devoted to your goal.
L'empressement et le sérieux sont primordiaux. La confiance vient avec l'expérience. Soyez dévoué à votre objectif.

807.If your desire and confidence are strong, they will operate and take you to your goal, for you will not cause delay by hesitation and compromise.
Si votre désir et votre confiance sont forts, ils fonctionneront et vous mèneront à votre but, car vous ne provoquerez pas de retard par l'hésitation et le compromis.

808.The greatest Guru is your inner Self. Truly, he is the supreme teacher. He alone can take you to your goal and he alone meets you at the end of the road. Confide in him and you need no outer Guru. But again you must have the strong desire to find him and do nothing that will create obstacles and delays. And do not waste energy and time on regrets. Learn from your mistakes and do not repeat them.
Le plus grand gourou est votre moi intérieur. En vérité, il est l’enseignant suprême. Lui seul peut vous conduire à votre but et lui seul vous rencontre au bout du chemin. Confiez-vous à lui et vous n'avez pas besoin d'un gourou extérieur. Mais là encore, vous devez avoir le vif désir de le retrouver et ne rien faire qui puisse créer des obstacles et des retards. Et ne perdez pas d'énergie et de temps à regretter. Apprenez de vos erreurs et ne les répétez pas.

809.Stop imagining, stop believing. See the contradictions, the incongruities, the falsehood and the sorrow of the human state, the need to go beyond.
Arrêtez d'imaginer, arrêtez de croire. Voir les contradictions, les incongruités, le mensonge et la douleur de l'état humain, la nécessité d'aller au-delà.

810.Freedom can not be gained nor kept without will-to-freedom. You must strive for liberation; the least you can do is uncover and remove the obstacles diligently. If you want peace you must strive for it.
La liberté ne peut être ni acquise ni conservée sans volonté de liberté. Vous devez vous efforcer de vous libérer ; le moins que vous puissiez faire est de découvrir et d'éliminer les obstacles avec diligence. Si vous voulez la paix, vous devez vous efforcer de l'obtenir.

811.All you need is a sincere longing of reality.
Il suffit d'avoir un désir sincère de réalité.

812.Questioner: How does one reach the Supreme State? Maharaj: By renouncing all lesser desires. as long as you are pleased with the lesser, you cannot have the highest.
Questionneur : Comment accéder à l'État suprême ?
Maharaj : En renonçant à tous les désirs inférieurs. Tant que vous êtes satisfait de l’inférieur, vous ne pouvez pas avoir le plus élevé.

813.Until you realize the unsatisfactoriness of everything, it transiency and limitation, and collect your energies in one great longing, even the first step is not made.
Tant que vous ne réalisez pas l'insatisfaction de tout, son caractère transitoire et limité, et que vous ne rassemblez pas vos énergies dans un grand désir, même le premier pas n'est pas fait.

814.Once you have grasped the truth that the world is full of suffering, that to be born is a calamity, you will find the urge and the energy to go beyond it.
Une fois que vous aurez saisi la vérité que le monde est plein de souffrance, que naître est une calamité, vous trouverez l'envie et l'énergie de la dépasser.

815.There must be the desire first. When the desire is strong, the willingness to try will come. You do not need assurance of success when the desire is strong.
Il faut d'abord que le désir existe. Quand le désir est fort, la volonté d'essayer vient. Vous n'avez pas besoin d'une assurance de réussite lorsque le désir est fort.

816.Suffering has made you dull, unable to see its enormity. Your first task is to see the sorrow in you and around you; your next to long intensely for liberation. The very intensity of longing will guide you; you need no other guide.
La souffrance vous a rendu terne, incapable de voir son énormité. Votre première tâche est de voir la tristesse en vous et autour de vous ; votre prochaine, de désirer intensément pour la libération. L'intensité même du désir vous guidera ; vous n'avez besoin d'aucun autre guide.

817.Questioner: How is the person removed? Maharaj: By determination. Understand that it must go and wish it to go - it shall go if you are earnest about it.
Questionneur : Comment la personne est-elle enlevée?
Maharaj : Par détermination. Comprenez qu'elle doit partir et souhaitez qu'elle parte - elle partira si vous êtes sérieux.

818.As long as you are interested in your present way of living you will not abandon it. Discovery cannot come as long as you cling to the familiar. It is only when you realize fully the immense sorrow of your life and revolt against it, that a way out can be found.
Tant que votre mode de vie actuel vous intéresse, vous ne l'abandonnerez pas. La découverte ne peut pas venir tant que vous vous accrochez à ce qui vous est familier. Ce n'est que lorsque l'on prend pleinement conscience de l'immense douleur de sa vie et que l'on se révolte contre elle, qu'une issue peut être trouvée.

819.The real is, behind and beyond words, incommunicable, directly experienced, explosive in its effects on the mind. It is easily had when nothing else is wanted.
Le réel est, derrière et au-delà des mots, incommunicable, directement vécu, explosif dans ses effets sur l'esprit. Il est facile de l'avoir quand on ne veut rien d'autre.



THE SUPREME YOGA
-------------------------------------------------------------------------------------
820.Abandon your latent tendencies even as a bird wishing to fly onto the sky breaks out of its shell. Born of ignorance, these tendencies are hard to destroy, and they give birth to endless sorrow. It is the ignorant self-limiting tendency of the mind that views the infinite as the finite. However, even as sun dispels mist, inquiry into the nature of the Self this ignorant self-limiting tendency. In fact, the very desire to undertake this inquiry is able to bring about a change. Austerities and such practices are of no use in this. When the mind is purified of its past by the arising of wisdom it abandons its previous tendencies. The mind seeks the Self only in order to dissolve itself in the Self. This indeed is in the very nature of the mind. This is the supreme goal; strive for this.
Abandonnez vos tendances latentes, tel un oiseau désireux de s'envoler vers le ciel sort de sa coquille. Nées de l'ignorance, ces tendances sont difficiles à détruire, et elles donnent naissance à une tristesse sans fin. C'est la tendance ignorante auto-limitante de l'esprit de considérer l'infini comme le fini. Cependant, de même que le soleil dissipe la brume, investiguez dans la nature du Soi cette tendance ignorante qui se limite elle-même. En fait, le désir même d'entreprendre cette enquête est capable de provoquer un changement. L'austérité et les pratiques de ce type ne sont d'aucune utilité. Lorsque l'esprit est purifié de son passé par l'émergence de la sagesse, il abandonne ses tendances antérieures. L'esprit ne cherche le Soi que pour se dissoudre dans le Soi. C'est en effet la nature même de l'esprit. C'est l'objectif suprême, il faut s'efforcer d'y parvenir.

821.Even as darkness disappears on turning towards light, ignorance disappears if you turn towards the light of the Self. As long as there does not arise a natural yearning for Self-knowledge, so long this ignorance or mental conditioning throws up an endless stream of world-appearance.
De même que l'obscurité disparaît quand on se tourne vers la lumière, l'ignorance disparaît quand on se tourne vers la lumière du Soi. Tant qu'il n'y a pas d'aspiration naturelle à la connaissance du Soi, cette ignorance ou ce conditionnement mental engendre un flux sans fin d'apparition du monde.

822.There arises the pure wish to attain liberation. This lead to serious inquiry. Then the mind becomes subtle because the inquiry thins out the mental conditioning. As a result of the rising of pure wisdom, one's consciousness moves in the reality. Then the mental conditioning vanishes and there is non-attachment. Bondage to actions and their fruits ceases. The vision is firmly established in truth and the apprehension of the unreal is weakened. Even while living and functioning in this world, he who has this unconditioned vision does what has to be done as if he is asleep, without thinking of the world and its pleasures. After some years of living like this, one is fully liberated and he transcends all these states; he is liberated while living.
C'est là que surgit le pur désir d'atteindre la libération. Cela conduit à une investigation sérieuse. L'esprit devient alors subtil ,car l’investigation amoindrie le conditionnement mental. Grâce à la montée de la sagesse pure, la conscience de l’individu se déplace dans la réalité. Ensuite, le conditionnement mental disparaît et il y a un non-attachement. La servitude aux actions et à leurs fruits cesse. La vision est fermement ancrée dans la vérité et l'appréhension de l'irréel est affaiblie. Même lorsqu'il vit et fonctionne dans ce monde, celui qui a cette vision inconditionnelle fait ce qui doit être fait comme s'il dormait, sans penser au monde et à ses plaisirs. Après quelques années à vivre ainsi, l’individu est pleinement libéré et transcende tous ces états ; il est libéré tout en étant vivant.

823.The Self being one and undivided, there is nothing else worth attaining or desiring. This Self undergoes no change and does not die.
Le Soi étant un et indivisible, il n'y a rien d'autre à atteindre ou à désirer. Ce Soi ne subit aucun changement et ne meurt pas.

824.In this world all things come into being and perish and therefore there is repeated experience of sorrow. All the pleasures of the world inevitably end in sorrow.
Dans ce monde, toutes les choses naissent et périssent et, par conséquent, il y a expérience répétée de la douleur. Tous les plaisirs du monde se terminent inévitablement dans la douleur.

825.By Self-knowledge rid yourself of the problems connected with the life hereafter. There is not time to lose for life is ebbing away all the time.
Par la connaissance du Soi, débarrassez-vous des problèmes liés à la vie dans l'au-delà. Il n'y a pas de temps à perdre, car la vie s'écourte sans cesse.

826.When one is knocked about by the troubles and tribulations of earthly existence and is "tired of all this", he seeks refuge from all this.
Quand on est frappé par les troubles et les tribulations de l'existence terrestre et qu'on est "fatigué de tout cela", on cherche un refuge pour échapper à tout cela.

827.One should carefully investigate the bliss of liberation and the sorrow inevitable to ignorance.
Il faut étudier avec soin la félicité de la libération et la douleur inévitable de l'ignorance.

828.Liberation confers "inner coolness" (peace) on the mind; bondage promotes psychological distress. Even after realizing this, one does not strive for liberation. How foolish people are! Such people are overcome by desire for sense-gratification. But even they can cultivate a desire for liberation by a study of this scripture.
La libération confère une "fraîcheur intérieure" (paix) à l'esprit ; l'asservissement favorise la détresse psychologique. Même après avoir pris conscience de cela, on ne cherche pas à se libérer. Que les gens sont idiots ! Ces personnes sont dépassées par le désir de gratification des sens. Mais elles ne peuvent cultiver un désir de libération par l'étude de cette écriture.



SRI SANKARA
-------------------------------------------------------------------------------------
829.Internal renunciation and external renunciation - it is the dispassionate man who is capable of these. The dispassionate man abandons fetters internal and external because of his yearning for liberation.
Le renoncement interne et le renoncement externe - c'est l'homme impartial qui en est capable. L'homme serein abandonne les entraves internes et externes en raison de son désir de libération.

830.When the force of the desire for Truth blossoms, Selfish desire wither away, just like darkness vanishes before the radiance of the light of dawn.
Lorsque la force du désir de Vérité s'épanouit, le désir égoïste se flétrit, tout comme l'obscurité disparaît devant le rayonnement de la lumière de l'aube.

831.By achieving the purity of an habitual discrimination and dispassion, the mind is inclined to liberation, so the wise seeker after liberation should first develop these.
En atteignant la pureté d'une habituelle discrimination et absence de passion, l'esprit est enclin à la libération, et donc le sage chercheur de la libération devrait d'abord les développer.

832.If you really have a desire for liberation, avoid the senses from a great distance, as you would poison, and continually practice the nectar-like qualities of contentment, compassion, forbearance, honesty, calm and restraint.
Si vous avez vraiment un désir de libération, évitez les sens d’une grande distance, comme si vous vous empoisonneriez, et pratiquez continuellement les qualités semblables à du nectar: le contentement, la compassion, la tolérance, l'honnêteté, le calme et la retenue.

833.The wise talk here of four qualities, possessed of which one will succeed, but without which one will fail. First is listed discrimination between unchanging and changing realities, and after that dispassion for the enjoyment of the fruits of action both here and hereafter, and then the group of six qualities including peace and of course the desire for liberation.
Le sage parle ici de quatre qualités, possédées par celui qui réussira, mais sans lesquelles on ne peut qu’échouer. On y trouve d'abord la discrimination entre les réalités immuables et les réalités changeantes, et ensuite l'absence de passion pour la jouissance des fruits de l'action maintenant et dans l'avenir, puis le groupe des six qualités comprenant la paix et, bien sûr, le désir de libération.

834.Dispassion is the turning away from what can be seen and heard and so on in everything which is impermanent.
L'absence de passion est le fait de se détourner de ce qui peut être vu et entendu et ainsi de suite de tout, lequel est impermanent.

835.The settling of the mind in its goal, by turning away from the mass of objects through observing their defects again and again, is known as peace.
L'enracinement de l'esprit dans son but, en se détournant de la masse des objets en observant leurs défauts encore et encore, est connue sous le nom de paix.

836.The established of the senses each in its own source by means of turning away from their objects is known as control. The supreme restrain is in the mind function not being involved in anything external.
L'établissement de chacun des sens dans sa propre source en les détournant de leurs objets est connu sous le nom de contrôle. La retenue suprême est la fonction de l'esprit qui n’est impliquée dans quoi que ce soit d'extérieur.

837.Bearing all afflictions without retaliation without mental disturbance is what is known as patience.
Supporter toutes les afflictions sans représaille et sans perturbation mentale est ce qu'on appelle la patience.

838.It is in a man who as strong dispassion and desire for liberation that peacefulness and so on are really fruitful.
C'est chez un homme qui a une forte absence de passion et un désir de libération que la tranquillité et ainsi de suite sont vraiment fructueuses.

839.Among the contributory factors of liberation, devotion stands supreme, and it is the search for one's own true nature that is meant by devotion.
Parmi les facteurs de libération, la dévotion est suprême, et c'est la recherche de sa propre vraie nature qui est visée par la dévotion.

840.The practice of faith, devotion and meditation are declared by scripture to be the means to liberation for a seeker after liberation. He who perseveres in these will achieve freedom from bondage to the body; created by ignorance.
La pratique de la foi, de la dévotion et de la méditation sont déclarées par les écritures comme étant le moyen de libération pour un chercheur de libération. Celui qui persévère dans cette voie s'affranchit de l'esclavage au corps, créé par l'ignorance.

841.Let all those who put away and cast aside every sin of thought, who are sated with this world's joys, whose thoughts are full of peace, who delight in words of wisdom, who rule themselves, who long to be free, draw near to this teaching, which is dedicated to them.
Que tous ceux qui mettent de côté et rejettent tous les péchés de la pensée, qui sont rassasiés des joies de ce monde, dont les pensées sont pleines de paix, qui se délectent des paroles de sagesse, qui se gouvernent eux-mêmes, qui aspirent à être libres, s'approchent de cet enseignement qui leur est dédié.

842.Renouncing inwardly, renouncing outwardly - this is possible only for him who is free from passion; and he who is free from passion renounces all attachment within and without. through the longing for freedom.
Renoncer intérieurement, renoncer extérieurement - cela n'est possible que pour celui qui est libéré de la passion ; et celui qui est libéré de la passion renonce à tout attachement intérieur et extérieur par l'aspiration à la liberté.

843.Four perfections are numbered by the wise. When they are present there is success, but in their absence is failure. First is counted the discernment between things lasting and unlasting. Next Dispassion, the indifference to self-indulgence. Then the six graces, beginning with restfulness. Then the longing for freedom.
Quatre perfections sont numérotées par les sages. Lorsqu'ils sont présentes, il y a du succès, mais en leur absence, c'est l'échec. On compte d'abord le discernement entre ce qui est durable et ce qui ne l'est pas. La prochaine est l'absence de passion, l'indifférence à l'indulgence du soi. Puis les six grâces, en commençant par le repos. Puis l'aspiration à la liberté.

844.A certainty like this - the eternal is real, the fleeting world is unreal; this that discernment between things lasting and unlasting.
Une certitude comme celle-ci - l'éternel est réel, le monde éphémère est irréel ; c'est ce discernement entre les choses durables et non durables.

845.And this is dispassion - a perpetual willingness to give up all sensual self-indulgence - everything lower than the eternal, through a constant sense of their insufficiency.
Et c'est cela l'absence de passion - une volonté perpétuelle de renoncer à toute complaisance sensuelle - tout ce qui est inférieur à l'éternel, par un sentiment constant de leur insuffisance.

846.Then the six graces: a steady intentness of the mind on its goal; this is restfulness.
Puis les six grâces : une intention constante de l'esprit sur son but ; c'est le repos.

847.The steadying of the powers that act and perceive, each in its own sphere, turning them back from sensuality; this is self-control.
La stabilisation des pouvoirs qui agissent et perçoivent, chacun dans sa propre sphère, les détournant de la sensualité ; c'est la maîtrise du soi.

848.The raising of the mind above external things; this is the true withdrawal.
L'élévation de l'esprit au-dessus des choses extérieures ; c'est le véritable retrait.

849.The enduring of all ills without petulance and without self-pity; this is right endurance.
L’endurance de tous les maux sans pétulance et sans apitoiement sur soi-même ; c'est la juste endurance.

850.The intentness of the soul on the pure eternal; this is right meditation.
L'intention de l'âme sur l'éternel pur ; c'est la juste méditation.

851.When dispassion and longing for Freedom are strong, then restfulness and the other graces will bear fruit.
Quand l'absence de passion et l'aspiration à la liberté sont fortes, alors le repos et les autres grâces porteront leurs fruits.

852.But when these two - dispassion and longing for freedom - are lacking, then restfulness and the other graces are a mere appearance.
Mais lorsque ces deux éléments - l'absence de passion et l'aspiration à la liberté - font défaut, alors le repos et les autres grâces ne sont qu'une apparence.






