import re
import sqlite3

conn = sqlite3.connect('SpiritualQuotes.db')

def step_to_file(step, filename):
    step_reformat = ""
    for sage, quotes in step.items():
        step_reformat += sage + "\n"
        for quote in quotes:
            step_reformat += quote + "\n"
        step_reformat += "\n"

    with open(filename, "w") as text_file:
        print(step_reformat, file=text_file)


def extract_quotes(step_file):
    quotes = []
    quotes.append("")
    with open(step_file, "r", encoding="utf8") as fp:
        line = fp.readline()
        while line:
            line = line.replace("\n", "").strip()
            if line == "":
                quotes.append("")
            else:
                quotes[-1] += " " + line
            line = fp.readline()
    return quotes

def save_quotes_to_db(quotes, lang):
    source = "Who am I?"
    if lang == "fr":
        source = "Qui suis-je?"
    c = conn.cursor()
    index_quote = 1
    for quote in quotes:
        try:

            sql_query = "INSERT INTO SpiritualQuotes (quote, author, source, language, chapter, number) VALUES ('%s', '%s', '%s', '%s', '%s', %s);" \
                % (quote.replace("'", "''"), "Ramana Maharshi", source, lang, "", str(index_quote))
            c.execute(sql_query)
        except Exception as e:
            print(e)
        index_quote = index_quote + 1
        if index_quote == 2 or index_quote == 6:
            index_quote = index_quote + 1
    conn.commit()

def who_am_i_to_db():
    quotes_en = extract_quotes("who_am_i.txt")
    quotes_fr = extract_quotes("who_am_i_fr.txt")
    save_quotes_to_db(quotes_en, "en")
    save_quotes_to_db(quotes_fr, "fr")

def main():
    who_am_i_to_db()
    conn.close()


if __name__ == '__main__':
    main()
