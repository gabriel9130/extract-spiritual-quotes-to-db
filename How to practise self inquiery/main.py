import re
import sqlite3

conn = sqlite3.connect('2_How_to_practise_self_inquiery.db')

def chapters_to_file(step, filename):
    step_reformat = ""
    for sage, quotes in step.items():
        step_reformat += sage + "\n"
        for quote in quotes:
            step_reformat += quote + "\n"
        step_reformat += "\n"

    with open(filename, "w") as text_file:
        print(step_reformat, file=text_file)


def extract_quotes(step_file):
    quotes_by_chapter = {}
    with open(step_file, "r", encoding="utf8") as fp:
        line = fp.readline()
        while line:
            line = line.replace("\n", "").strip()
            if line != "":
                if re.match("-{3,}", line):  # new chapter
                    line = re.sub("-{2,}", "", line).strip()
                    quotes_by_chapter[line] = []
                    current_chapter = line
                else:  # quote
                    if re.match("^[0-9]+.", line):  # new quote
                        line = re.sub("^[0-9]+.", "", line).strip()
                        quotes_by_chapter[current_chapter].append(line)
                    else:  # the rest of the previous quote
                        quotes_by_chapter[current_chapter][-1] += " " + line
            line = fp.readline()
    return quotes_by_chapter


def save_quotes_to_db(step_quotes, lang):
    index_quote = 1
    book_title = "How to practise Self-inquiery"
    if lang == "fr":
        book_title = "Comment pratiquer l''investigation du Soi"
    for chapter, quotes in step_quotes.items():
        for quote in quotes:
            try:
                sql_query = "INSERT INTO SpiritualQuotes (quote, author, source, language, chapter, number) VALUES ('%s', '%s', '%s', '%s', '%s', %s);" \
                            % (quote.replace("'", "''"), "Ramana Maharshi", book_title, lang, chapter.title(), str(index_quote))
                conn.execute(sql_query)
            except Exception as e:
                print(e)
            index_quote = index_quote + 1
    conn.commit()


def how_to_practise_self_inquiery_to_db():
    chapter_quotes_en = extract_quotes("how_to_practise_self_inquiery.txt")
    chapter_quotes_fr = extract_quotes("how_to_practise_self_inquiery_fr.txt")
    save_quotes_to_db(chapter_quotes_en, "en")
    save_quotes_to_db(chapter_quotes_fr, "fr")


def main():
    how_to_practise_self_inquiery_to_db()
    conn.close()


if __name__ == '__main__':
    main()
